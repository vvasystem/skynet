<?php

use Model\ServicesRepository;
use Model\TarifsRepository;
use Service\TarifService;

$di['services_repository'] = function ($c) {
 return new ServicesRepository($c['db_connect']);
};

$di['tarifs_repository'] = function ($c) {
    return new TarifsRepository($c['db_connect']);
};

$di['service.tarifs'] = function ($c) {
    return new TarifService($c['services_repository'], $c['tarifs_repository']);
};


