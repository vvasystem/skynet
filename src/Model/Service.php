<?php

namespace Model;

class Service
{

    /** @var int */
    private $id;

    /** @var int */
    private $userId;

    /** @var int */
    private $tarifId;

    /** @var string */
    private $payday;

    public function __construct(int $id, int $userId, int $tarifId, string $payday)
    {
        $this->id      = $id;
        $this->userId  = $userId;
        $this->tarifId = $tarifId;
        $this->payday  = $payday;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getTarifId(): int
    {
        return $this->tarifId;
    }

    public function getPayDay(): string
    {
        return $this->payday;
    }

}