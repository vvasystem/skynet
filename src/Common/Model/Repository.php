<?php

namespace Common\Model;

abstract class Repository
{

    /** @var \PDO */
    protected $pdoConnection;

    public function __construct(\PDO $pdoConnection)
    {
        $this->pdoConnection = $pdoConnection;
    }

}