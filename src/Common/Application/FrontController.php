<?php

namespace Common\Application;

use Common\Application\Exception\RouteNotFoundException;

class FrontController
{

    /** @var UrlMatcherInterface */
    private $urlMatcher;

    /** @var RouterCollectionInterface */
    private $router;

    public function __construct(UrlMatcherInterface $urlMatcher, RouterCollectionInterface $router)
    {
        $this->urlMatcher = $urlMatcher;
        $this->router     = $router;
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws RouteNotFoundException
     */
    public function dispatch(RequestInterface $request): ResponseInterface
    {
        $url = $request->getPathInfo();
        $routers = $this->router->getRoutersByMethod($request->getMethod());
        foreach ($routers as $router) {
            if ($this->urlMatcher->match($url, $router->getPattern())) {
                $response = new Response();
                $response->setBody(\json_encode(array_merge(['result' => 'ok'], $router->handle($request)), \JSON_UNESCAPED_UNICODE));
                return $response;
            }
        }

        throw new RouteNotFoundException(\sprintf('Not found route for url %s', $url));
    }

}