<?php

namespace Common\Application;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ApplicationProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $di)
    {
        $di['app'] = function ($c) {
            return new Application($c);
        };
    }

}