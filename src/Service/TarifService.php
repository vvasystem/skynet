<?php

namespace Service;

use Model\ServicesRepository;
use Model\Tarif;
use Model\TarifsRepository;

class TarifService
{

    /** @var ServicesRepository */
    private $servicesRepository;

    /** @var TarifsRepository */
    private $tarifsRepository;

    public function __construct(ServicesRepository $servicesRepository, TarifsRepository $tarifsRepository)
    {
        $this->servicesRepository = $servicesRepository;
        $this->tarifsRepository   = $tarifsRepository;
    }

    public function getByUserIdServiceId(int $userId, int $serviceId): array
    {
        $service = $this->servicesRepository->findById($serviceId);
        $tarif = $this->tarifsRepository->findById($service->getTarifId());
        $tarifs = $this->tarifsRepository->findByGroupId($tarif->getGroupId());
        return [
            'tarifs' => [
                'title'  => $tarif->getTitle(),
                'link'   => $tarif->getLink(),
                'speed'  => $tarif->getSpeed(),
                'tarifs' => $this->getTarifsForApi($tarifs),
            ]
        ];
    }

    public function addTarif(int $userId, int $serviceId, int $tarifId): array
    {
        $tarif = $this->tarifsRepository->findById($tarifId);
        $this->servicesRepository->setTarifByUserIdAndServiceId($userId, $serviceId, $tarif->getId());
        return [];
    }

    /**
     * @param Tarif[] $tarifs
     * @return array
     */
    private function getTarifsForApi(array $tarifs): array
    {
        $result = [];
        foreach ($tarifs as $tarif) {
            $result[] = [
                'ID'         => $tarif->getId(),
                'title'      => $tarif->getTitle(),
                'price'      => $tarif->getPrice(),
                'pay_period' => $tarif->getPayPeriod(),
                'new_payday' => $tarif->getNewPayDay(),
                'speed'      => $tarif->getSpeed(),
            ];
        }

        return $result;
    }

}