<?php

namespace Model;

use Common\Model\Repository;

class ServicesRepository extends Repository
{

    public function findById(int $id): Service
    {
        $statement = $this->pdoConnection->prepare('SELECT `ID`, `user_id`, `tarif_id`, `payday` FROM `services` WHERE `ID` = :service_id');
        $statement->execute([':service_id' => $id]);
        $row = $statement->fetch(\PDO::FETCH_NAMED);
        if ($row) {
            return new Service($row['ID'], $row['user_id'], $row['tarif_id'], $row['payday']);
        }

        throw new \DomainException(\sprintf('Service with id `%s` not found. ', $id));
    }

    public function setTarifByUserIdAndServiceId(int $userId, int $serviceId, int $tarifId)
    {
        $statement = $this->pdoConnection->prepare('UPDATE `services` SET `tarif_id` = :tarif_id, `payday` = :payday WHERE `ID` = :service_id AND `user_id` = :user_id');
        $statement->execute([
            ':service_id' => $serviceId,
            ':user_id'    => $userId,
            ':tarif_id'   => $tarifId,
            ':payday'     => \date('Y-m-d', \time()),
        ]);
    }

}