<?php

namespace Common\Application;

class Request implements RequestInterface
{

    private $method;
    private $pathInfo;

    private $get;
    private $post;

    public function __construct(array $server = [], array $get = [], array $post = [])
    {
        $this->method   = \strtoupper($server['REQUEST_METHOD']);
        $this->pathInfo = $server['REQUEST_URI'];
        $this->get      = \array_merge($get, $this->urlToParams($this->pathInfo));
        $this->post     = \array_merge($post, $this->getPutData());
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getPathInfo(): string
    {
        return $this->pathInfo;
    }

    public function get($name = null, $default_value = null)
    {
        return $this->getVariable($this->get, $name, $default_value);
    }

    public function post($name = null, $default_value = null)
    {
        return $this->getVariable($this->post, $name, $default_value);
    }

    private function getVariable(array $array = [], $name = null, $default_value = null)
    {
        if (empty($name)) {
            return $array;
        }

        return $array[$name] ?? $default_value;
    }

    private function urlToParams(string $url): array
    {
        $urlPieces = explode('/', $url);
        $count = 0;
        $results = [];
        foreach ($urlPieces as $key => $value) {
            if(0 !== $count % 2){
                $results[$urlPieces[$key]] = $urlPieces[$count+1];
            }
            $count++;
        }

        return $results;
    }

    private function getPutData(): array
    {
        $rawData = \file_get_contents('php://input');
        $boundary = substr($rawData, 0, strpos($rawData, "\r\n"));

        if(empty($boundary)){
            parse_str($rawData, $data);
            return $data;
        }

        return [];
    }

}