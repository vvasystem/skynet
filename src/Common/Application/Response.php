<?php

namespace Common\Application;

class Response implements ResponseInterface
{

    const STATUS_CODE_OK          = 200;
    const STATUS_CODE_BAD_REQUEST = 400;
    const STATUS_CODE_NOT_FOUND   = 404;

    protected $status = self::STATUS_CODE_OK;
    protected $body = '';

    public function setStatus(int $code)
    {
        $this->status = $code;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setBody(string $string)
    {
        $this->body = $string;
    }

    public function getBody(): string
    {
        return $this->body;
    }

}