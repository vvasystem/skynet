<?php

namespace Common\Application;

interface UrlMatcherInterface
{

    public function match(string $url, string $pattern): bool;

}