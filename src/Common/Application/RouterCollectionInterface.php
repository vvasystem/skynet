<?php

namespace Common\Application;

interface RouterCollectionInterface
{

    const GET_METHOD = 'GET';
    const PUT_METHOD = 'PUT';

    /**
     * @param string $method
     * @return Router[]
     */
    public function getRoutersByMethod(string $method): array;

}