<?php

namespace Model;

use Common\Model\Repository;

class TarifsRepository extends Repository
{

    public function findById(int $id): Tarif
    {
        $statement = $this->pdoConnection->prepare('SELECT `ID`, `title`, `price`, `link`, `speed`, `pay_period`, `tarif_group_id` FROM `tarifs` WHERE `ID` = :tarif_id');
        $statement->execute([':tarif_id' => $id]);
        $row = $statement->fetch(\PDO::FETCH_NAMED);
        if ($row) {
            return $this->getModelByRow($row);
        }

        throw new \DomainException(\sprintf('Tarif with id `%s` not found. ', $id));
    }

    /**
     * @param int $groupId
     * @return Tarif[]
     */
    public function findByGroupId(int $groupId): array
    {
        $statement = $this->pdoConnection->prepare('SELECT `ID`, `title`, `price`, `link`, `speed`, `pay_period`, `tarif_group_id` FROM `tarifs` WHERE `tarif_group_id` = :group_id');
        $statement->execute([':group_id' => $groupId]);
        $rows = $statement->fetchAll(\PDO::FETCH_NAMED);
        return \array_map(function (array $value) {
            return $this->getModelByRow($value);
        }, $rows);
    }

    private function getModelByRow(array $row): Tarif
    {
        return new Tarif($row['ID'], $row['title'], $row['price'], $row['link'], $row['speed'], $row['pay_period'], $row['tarif_group_id']);
    }

}