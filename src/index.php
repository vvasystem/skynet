<?php

use Common\Application\Application;
use Common\Application\ApplicationProvider;
use Common\Application\DBProvider;
use Common\Application\Request;
use Common\Application\RouterProvider;
use Pimple\Container;

require_once __DIR__ . '/db_cfg.php';
require_once __DIR__ . '/autoloader.php';

$di = new Container();
$di->register(new ApplicationProvider());
$di->register(new DBProvider());
$di->register(new RouterProvider());

require_once __DIR__ . '/routers.php';
require_once __DIR__ . '/services.php';

/** @var Application $app */
$app = $di['app'];
$app->run(new Request($_SERVER, $_GET, $_POST));