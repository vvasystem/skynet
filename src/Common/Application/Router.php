<?php

namespace Common\Application;

class Router
{

    /** @var string */
    private $pattern;

    /**@var callable */
    private $callback;

    public function __construct(string $pattern, Callable $callback)
    {
        $this->pattern  = $pattern;
        $this->callback = $callback;
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }

    public function handle(RequestInterface $request): array
    {
        return \call_user_func($this->callback, $request);
    }

}