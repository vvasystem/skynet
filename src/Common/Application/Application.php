<?php

namespace Common\Application;

use Pimple\Container;

class Application
{

    /** @var Container */
    private $di;

    public function __construct(Container $di)
    {
        $this->di = $di;
    }

    public function run(RequestInterface $request)
    {
        try {
            /** @var FrontController $frontController */
            $frontController = $this->di['front_controller'];
            $response = $frontController->dispatch($request);
        } catch (\Throwable $e) {
            $response = new Response();
            $response->setStatus(Response::STATUS_CODE_BAD_REQUEST);
            $response->setBody(\json_encode(['result' => 'error', 'message' => $e->getMessage()], \JSON_UNESCAPED_UNICODE));
        }
        $this->sendResponse($response);
    }

    private function sendResponse(ResponseInterface $response)
    {
        \header('Content-type:application/json; charset=UTF-8');
        \http_response_code($response->getStatus());
        echo $response->getBody();
    }

}