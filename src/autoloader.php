<?php

class SimpleLoader
{
    public static function loadClass($class)
    {
        include \str_replace('\\', '/', $class) . '.php';
    }
}

\spl_autoload_register(['SimpleLoader', 'loadClass'], true, true);