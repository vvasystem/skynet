<?php

use Common\Application\RequestInterface;
use Common\Application\RouteCollection;
use Common\Application\Router;
use Service\TarifService;

/** @var RouteCollection  $routeCollection */
$routeCollection = $di['route_collection'];
$routeCollection->addGetRoute(new Router('/users/{user_id}/services/{service_id}/tarifs', function (RequestInterface $request) use ($di) {
    /** @var TarifService $tarifService */
    $tarifService = $di['service.tarifs'];
    return $tarifService->getByUserIdServiceId((int)$request->get('users'), (int)$request->get('services'));
}));
$routeCollection->addPutRoute(new Router('/users/{user_id}/services/{service_id}/tarif', function (RequestInterface $request) use ($di) {
    /** @var TarifService $tarifService */
    $tarifService = $di['service.tarifs'];
    return $tarifService->addTarif((int)$request->get('users'), (int)$request->get('services'), (int)$request->post('tarif_id'));
}));
