<?php

namespace Common\Application;

class RouteCollection implements RouterCollectionInterface
{

    /** @var Router[][] */
    private $routers = [];

    /**
     * @inheritDoc
     */
    public function addGetRoute(Router $router)
    {
        $this->routers[static::GET_METHOD][] = $router;
    }

    /**
     * @inheritDoc
     */
    public function addPutRoute(Router $router)
    {
        $this->routers[static::PUT_METHOD][] = $router;
    }

    /**
     * @inheritDoc
     */
    public function getRoutersByMethod(string $method): array
    {
        return $this->routers[$method] ?? [];
    }

}