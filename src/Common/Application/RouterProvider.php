<?php

namespace Common\Application;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class RouterProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $di)
    {
        $di['route_collection'] = function ($c) {
            return new RouteCollection();
        };

        $di['url_matcher'] = function ($c) {
            return new UrlMatcher();
        };

        $di['front_controller'] = function ($c) {
            return new FrontController($c['url_matcher'], $c['route_collection']);
        };
    }

}