<?php

namespace Common\Application;

class UrlMatcher implements UrlMatcherInterface
{

    public function match(string $url, string $pattern): bool
    {
        $pattern = \preg_replace('#\{\w+\}#', '\w+', $pattern);
        \preg_match(\sprintf('#^%s$#', $pattern), $url, $matchers);
        return !empty($matchers);
    }

}