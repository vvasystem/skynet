<?php

namespace Common\Application;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class DBProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $di)
    {
        $di['db_connect'] = function ($c) {
            return new \PDO(
                \sprintf('mysql:dbname=%s;host=%s;port=3306', \DB_NAME, \DB_HOST),
                \DB_USER,
                \DB_PASSWORD,
                [
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                ]
            );
        };
    }

}