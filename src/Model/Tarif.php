<?php

namespace Model;

class Tarif
{

    /** @var int */
    private $id;

    /** @var string */
    private $title;

    private $price;

    /** @var string */
    private $link;

    private $speed;

    private $payPeriod;

    /** @var int */
    private $tarifGroupId;

    public function __construct(int $id, string $title, $price, string $link, $speed, $payPeriod, int $tarifGroupId)
    {
        $this->id     = $id;
        $this->title  = $title;
        $this->price  = $price;
        $this->link   = $link;
        $this->speed  = $speed;
        $this->payPeriod    = $payPeriod;
        $this->tarifGroupId = $tarifGroupId;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function getPayPeriod()
    {
        return $this->payPeriod;
    }

    public function getGroupId(): int
    {
        return $this->tarifGroupId;
    }

    public function getNewPayDay(): int
    {
        return \strtotime(\date('d.m.Y 23:59:59', \time())) + $this->payPeriod * 60 * 60 * 24;
    }

}